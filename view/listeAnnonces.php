<?php
/**
 * Author : Nathan Rayburn
 * Date : 09/06/21
 */
ob_start();
$titre="Accueil";

?>
<section class="body-font">
    <div class="container px-5 py-24 mx-auto">
        <div class="flex flex-wrap -m-4">
            <div class="p-4 md:w-1/3">
                <div class="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                    <img class="lg:h-48 md:h-36 w-full object-contain object-center" src="img/chimie.jpg"
                         alt="blog">
                    <div class="p-6">
                        <h2 class="tracking-widest text-xs title-font font-medium text-red-500 mb-1">Chimie</h2>
                        <h1 class="title-font text-lg font-medium text-yellow-500 mb-3">Chimie preparation au Bac et à la maturité</h1>
                        <p class="text-white leading-relaxed mb-3">Etat : neuf</p>
                        <p class="text-white leading-relaxed mb-3">Livres de Chimie pour les cours de maturité. Il n'a pas beaucoup été utilisé, il est comme neuf.</p>
                        <div class="flex items-center flex-wrap ">
                            <a id="multiplix" class="font-bold text-yellow-500 hover:text-blue-500 inline-flex items-center md:mb-2 lg:mb-0 cursor-pointer" >Contacter le vendeur
                                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"
                                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M5 12h14"></path>
                                    <path d="M12 5l7 7-7 7"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-4 md:w-1/3">
                <div class="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                    <img class="lg:h-48 md:h-36 w-full object-contain object-center" src="img/maths_trigonometrie.jpg"
                         alt="blog">
                    <div class="p-6">
                        <h2 class="tracking-widest text-xs title-font font-medium text-red-500 mb-1">Mathématique</h2>
                        <h1 class="title-font text-lg font-medium text-yellow-500 mb-3">
                            TRIGONOMETRIE, GEOMETRIE VECTORIELLE</h1>
                        <p class="text-white leading-relaxed mb-3">Etat : neuf</p>
                        <p class="text-white leading-relaxed mb-3">Livres de maths pour les cours de maturité. Il n'a pas beaucoup été utilisé, il est comme neuf.</p>
                        <div class="flex items-center flex-wrap ">
                            <a id="multiplix" class="font-bold text-yellow-500 hover:text-blue-500 inline-flex items-center md:mb-2 lg:mb-0 cursor-pointer" >Contacter le vendeur
                                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"
                                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M5 12h14"></path>
                                    <path d="M12 5l7 7-7 7"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-4 md:w-1/3">
                <div class="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                    <img class="lg:h-48 md:h-36 w-full object-contain object-center" src="https://dummyimage.com/720x400"
                         alt="blog">
                    <div class="p-6">
                        <h2 class="tracking-widest text-xs title-font font-medium text-red-500 mb-1">Mathématique</h2>
                        <h1 class="title-font text-lg font-medium text-yellow-500 mb-3">Chimie preparation au Bac et à la maturité</h1>
                        <p class="text-white leading-relaxed mb-3">Etat : neuf</p>
                        <p class="text-white leading-relaxed mb-3">Livres de Chimie pour les cours de maturité. Il n'a pas beaucoup été utilisé, il est comme neuf.</p>
                        <div class="flex items-center flex-wrap ">
                            <a id="multiplix" class="font-bold text-yellow-500 hover:text-blue-500 inline-flex items-center md:mb-2 lg:mb-0 cursor-pointer" >Contacter le vendeur
                                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"
                                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M5 12h14"></path>
                                    <path d="M12 5l7 7-7 7"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-4 md:w-1/3">
                <div class="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                    <img class="lg:h-48 md:h-36 w-full object-contain object-center" src="https://dummyimage.com/720x400"
                         alt="blog">
                    <div class="p-6">
                        <h2 class="tracking-widest text-xs title-font font-medium text-red-500 mb-1">Mathématique</h2>
                        <h1 class="title-font text-lg font-medium text-yellow-500 mb-3">Chimie preparation au Bac et à la maturité</h1>
                        <p class="text-white leading-relaxed mb-3">Etat : neuf</p>
                        <p class="text-white leading-relaxed mb-3">Livres de Chimie pour les cours de maturité. Il n'a pas beaucoup été utilisé, il est comme neuf.</p>
                        <div class="flex items-center flex-wrap ">
                            <a id="multiplix" class="font-bold text-yellow-500 hover:text-blue-500 inline-flex items-center md:mb-2 lg:mb-0 cursor-pointer" >Contacter le vendeur
                                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"
                                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M5 12h14"></path>
                                    <path d="M12 5l7 7-7 7"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-4 md:w-1/3">
                <div class="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                    <img class="lg:h-48 md:h-36 w-full object-contain object-center" src="https://dummyimage.com/720x400"
                         alt="blog">
                    <div class="p-6">
                        <h2 class="tracking-widest text-xs title-font font-medium text-red-500 mb-1">Mathématique</h2>
                        <h1 class="title-font text-lg font-medium text-yellow-500 mb-3">Chimie preparation au Bac et à la maturité</h1>
                        <p class="text-white leading-relaxed mb-3">Etat : neuf</p>
                        <p class="text-white leading-relaxed mb-3">Livres de Chimie pour les cours de maturité. Il n'a pas beaucoup été utilisé, il est comme neuf.</p>
                        <div class="flex items-center flex-wrap ">
                            <a id="multiplix" class="font-bold text-yellow-500 hover:text-blue-500 inline-flex items-center md:mb-2 lg:mb-0 cursor-pointer" >Contacter le vendeur
                                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"
                                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M5 12h14"></path>
                                    <path d="M12 5l7 7-7 7"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-4 md:w-1/3">
                <div class="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                    <img class="lg:h-48 md:h-36 w-full object-contain object-center" src="https://dummyimage.com/720x400"
                         alt="blog">
                    <div class="p-6">
                        <h2 class="tracking-widest text-xs title-font font-medium text-red-500 mb-1">Mathématique</h2>
                        <h1 class="title-font text-lg font-medium text-yellow-500 mb-3">Chimie preparation au Bac et à la maturité</h1>
                        <p class="text-white leading-relaxed mb-3">Etat : neuf</p>
                        <p class="text-white leading-relaxed mb-3">Livres de Chimie pour les cours de maturité. Il n'a pas beaucoup été utilisé, il est comme neuf.</p>
                        <div class="flex items-center flex-wrap ">
                            <a id="multiplix" class="font-bold text-yellow-500 hover:text-blue-500 inline-flex items-center md:mb-2 lg:mb-0 cursor-pointer" >Contacter le vendeur
                                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"
                                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M5 12h14"></path>
                                    <path d="M12 5l7 7-7 7"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$contenu = ob_get_clean();
require "body.php";
?>
