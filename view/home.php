<?php
/**
 * Author : Nathan Rayburn
 * Date : 09/06/21
 */
ob_start();
$titre="Accueil";

?>
<div class="flex xl:pt-12 md:pt-12 lg:pt-12 md:mt-2 pb-24 md:pb-5 place-content-center">
    <div class="w-full lg:w-3/4 xl:w-1/2 p-6 my-52">
        <!--Metric Card-->
        <div class="bg-gray-900 rounded shadow-2xl border-b-4 border-green-500 rounded-lg shadow-xl p-5 text-white">
            <div><p class="text-white text-2xl mt-10">Recherche d'annonces</p></div>
            <div class="mx-auto lg:mx-0 w-full pt-3 border-b-2 border-green-400 opacity-60"></div>
            <div class="flex flex-col md:flex-row lg:flex-row xl:flex-row place-content-center">
                <div class="m-5">
                    <div class="dropdown inline-block relative">
                        <button class="bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
                            <span class="mr-1">Filière</span>
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/> </svg>
                        </button>
                        <ul class="dropdown-menu absolute hidden text-gray-700 pt-1">
                            <li class=""><a class="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Informatique</a></li>
                            <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Automatique</a></li>
                            <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Électronique</a></li>
                            <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Polymechanique</a></li>
                            <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Mediamatique</a></li>
                            <li class=""><a class="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Employé de commerce</a></li>
                        </ul>
                    </div>
                        <div class="dropdown inline-block relative">
                            <button class="bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
                                <span class="mr-1">Branche</span>
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/> </svg>
                            </button>
                            <ul class="dropdown-menu absolute hidden text-gray-700 pt-1">
                                <li class=""><a class="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Mathématique</a></li>
                                <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Anglais</a></li>
                                <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Français</a></li>
                                <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Allemand</a></li>
                                <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Chimie</a></li>
                                <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Physique</a></li>
                                <li class=""><a class="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">Economie</a></li>
                            </ul>
                        </div>
                    <div class="dropdown inline-block relative">
                        <button class="bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
                            <span class="mr-1">Etat</span>
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/> </svg>
                        </button>
                        <ul class="dropdown-menu absolute hidden text-gray-700 pt-1">
                            <li class=""><a class="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">neuf</a></li>
                            <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">comme neuf</a></li>
                            <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">bon état</a></li>
                            <li class=""><a class="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">légérement abimé</a></li>
                            <li class=""><a class="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">abimé</a></li>
                        </ul>
                    </div>
                </div>
                <div class="m-5">
                    <div class="mx-auto max-w-sm">
                        <div class="">
                            <div class="flex items-center mr-4 mb-4">
                                <input id="radio1" type="radio" name="radio" class="hidden" checked />
                                <label for="radio1" class="flex items-center cursor-pointer text-base">
                                    <span class="w-8 h-8 inline-block mr-2 rounded-full border border-grey flex-no-shrink"></span>
                                    CFC</label>
                            </div>
                            <div class="flex items-center mr-4 mb-4">
                                <input id="radio2" type="radio" name="radio" class="hidden" />
                                <label for="radio2" class="flex items-center cursor-pointer text-base">
                                    <span class="w-8 h-8 inline-block mr-2 rounded-full border border-grey flex-no-shrink"></span>
                                    Maturité</label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="m-5">
                <a href="index.php?action=annoncesRecherche"> <div class="flex items-center justify-between mt-8">
                    <button type="submit" class="bg-yellow-600 text-gray-100 p-4 w-full rounded-2xl tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-yellow-400
                                        shadow-lg">
                        Rechercher
                    </button>

                </div>  </a>
            </div>
        </div>
        <!--/Metric Card-->
    </div>
</div>


<?php
$contenu = ob_get_clean();
require "body.php";
?>
