<?php
/**
 * Author : Nathan Rayburn
 * Date : 09/06/21
 */
ob_start();
$titre="Création d'annonce";

?>

<div class="max-w-4xl flex items-center flex-wrap mx-auto my-32">
  <!--Main Col-->
  <div id="profile" class="w-full rounded-lg shadow-2xl bg-gray-400 mx-6 py-8 px-16 lg:mx-0">
    <div class="p-4 md:p-12 text-center">
      <h1 class="text-5xl font-bold">Nouvelle annonce</h1>
    </div>

    <div class="font-bold pl-2">Matériel à vendre:</div>
    <div class="w-full border-2 p-6 mb-6 rounded-lg">
      <div class="">
        <label class="mr-2" for="filiere">Filière:</label>
        <select class="bg-gray-500" name="filiere" id="filiere">
          <option value="Automatique">Automatique</option>
          <option value="Électronique">Électronique</option>
          <option value="Informatique">Informatique</option>
          <option value="Mediamatique">Mediamatique</option>
          <option value="Polymechanique">Polymechanique</option>
          <option value="Commerce">Employé de commerce</option>
        </select>
      </div><br>
      <div>
        <input type="radio" id="CFC" name="matu" value="0">
        <label for="CFC" class="pr-8" checked >CFC</label>
        <input type="radio" id="Matu" name="matu" value="1">
        <label for="Matu">Maturité professionnelle</label>
      </div><br>
      <div class="">
        <label class="mr-2" for="branche">Branche:</label>
        <select class="bg-gray-500" name="branche" id="branche">
          <option value="Mathématique">Mathématique</option>
          <option value="Anglais">Anglais</option>
          <option value="Français">Français</option>
          <option value="Allemand">Allemand</option>
          <option value="Chimie">Chimie</option>
          <option value="Physique">Physique</option>
          <option value="Economie">Economie</option>
          <option value="metier">CFC</option>
        </select>
      </div><br>
      <div class="">
        <label class="mr-2" for="Objets">Objets:</label>
        <select class="bg-gray-500" name="Objets" id="object">
          <option value="1">livre de mathématique mpt 1</option>
          <option value="2">formulaire xxx</option>
          <option value="3">calculatrice TI-nspire cx</option>
          <option value="3">calculatrice TI-30</option>
        </select>
      </div>
      <small>Si le livre que vous souhaitez vendre n'est pas disponible c'est que le nombre maximum de ventes a été dépassé.</small>
    </div>
    <div class="font-bold pl-2">États:</div>
    <select class="mb-6 rounded-lg" name="etats" id="etats">
          <option value="1">neuf</option>
          <option value="2">comme neuf</option>
          <option value="3">bon état</option>
          <option value="3">légérement abimé</option>
          <option value="2">Abimé</option>
    </select>
    <div class="font-bold pl-2">Description:</div>
    <textarea class="w-full p-2 mb-6 rounded-lg" name="" id="" rows="8
    "></textarea>
    <div class="font-bold pl-2 mr-2">Prix (CHF):</div>
    <input class="rounded-lg" type="number" name="someid" /><br>
    <div class="mt-6 right-0">
      <button class="p-2 border-2 border-gray-800 rounded-lg shadow-2xl bg-green-500 hover:bg-green-500">Soummetre l'annonce</button>
      <button class="p-2 border-2 border-gray-800 rounded-lg shadow-2xl bg-red-500 hover:bg-red-500">Annuler</button>
    </div>
  </div>
</div>


<?php
$contenu = ob_get_clean();
require "body.php";

?>