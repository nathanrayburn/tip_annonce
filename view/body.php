<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CPNV Annonces</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="css/tailwind.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/body.js"></script>
</head>
<nav id="nav" class="fixed top-0 w-full flex flex-wrap h-auto items-center justify-between p-5 bg-gray-300">
    <img src="img/cpnv.png" class="h-12 w-52 object-contain" alt="CPNV ANNONCES">
    <div class="text-xl font-bold">CPNV Annonces</div>
    <div class="flex md:hidden">
        <button id="hamburger">
            <svg xmlns="http://www.w3.org/2000/svg" class="toggle block h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 4h13M3 8h9m-9 4h6m4 0l4-4m0 0l4 4m-4-4v12" />
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" class="toggle hidden h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 4h13M3 8h9m-9 4h9m5-4v12m0 0l-4-4m4 4l4-4" />
            </svg>
        </button>
    </div>
    <div class="toggle hidden md:flex w-full md:w-auto text-right text-bold mt-5 md:mt-0 border-t-2 border-blue-900 md:border-none">
        <a href="index.php?action=home" class="block md:inline-block font-bold text-green-600 hover:text-yellow-500 px-3 py-3 border-b-2 border-green-900 md:border-none">Accueil</a>
        <a href="index.php?action=annonces" class="block md:inline-block font-bold text-green-600 hover:text-yellow-500  px-3 py-3 border-b-2 border-green-900 md:border-none">Annonces</a>
        <a href="index.php?action=nouvelle_annonce" class="block md:inline-block font-bold text-green-600 hover:text-yellow-500  px-3 py-3 border-b-2 border-green-900 md:border-none">Créer une annonce</a>
        <a href="#" class="block md:inline-block font-bold text-green-600 hover:text-yellow-500  px-3 py-3 border-b-2 border-green-900 md:border-none">Mon Compte</a>
    </div>
</nav>
<body class="font-sans antialiased text-gray-900 leading-normal tracking-wider bg-gray-800">
<?=$contenu;?>
</body>

</html>
