<?php
/**
 * Author : Nathan Rayburn
 * Date : 3/18/21
 * Description : This file is designed to navigate data between the model and view
 */

/**
 * This function is designed to display the portfolio
 */
function home() {
    require "view/home.php";
}
function annonces()
{
    require "view/listeAnnonces.php";
}
function annoncesRechercher()
{
    require "view/listeAnnoncesRercherche.php";
}

function newannonce()
{
    require "view/creation_annonce.php";
}
