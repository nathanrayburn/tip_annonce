$(document).ready(function(){
    //hide and unhide hamburger for the navigation bar
    $("#hamburger").click(function()
    {
        const navToggle = document.getElementsByClassName("toggle");
        for (let i = 0; i < navToggle.length; i++) {
            navToggle.item(i).classList.toggle("hidden");
        }
    })
});
