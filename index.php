<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file will be the index of the whole web-site
 */
session_start();
require 'controller/controller.php';

if(isset($_GET["action"]) || isset($_POST["action"]))
{
    if(isset($_POST["action"]))
    {
        $_GET["action"] = $_POST["action"];
    }
    switch($_GET["action"])
    {
        case "home":
            home();
            break;
        case "annonces";
            annonces();
            break;
        case "annoncesRecherche";
            annoncesRechercher();
            break;
        case "annonce1":
            break;
        case "annonce2":
            break;
        case "annonce3":
            break;
        case "nouvelle_annonce":
            newannonce();
            break;
        default:
            home();
            break;
    }
}
else
{
    home();
}
